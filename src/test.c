#define _DEFAULT_SOURCE
#include "test.h"

#include <unistd.h>

#include "mem_internals.h"
#include "util.h"

static size_t pages_count(size_t const mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t const mem) { return getpagesize() * pages_count(mem); }

static size_t region_actual_size(size_t const query) { return size_max(round_pages(size_from_capacity((block_capacity){query}).bytes), REGION_MIN_SIZE); }

void test_1() {
    void *heap = heap_init(REGION_MIN_SIZE);
    size_t const heap_size = region_actual_size(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *test = _malloc(2048);
    debug_heap(stdout, heap);

    if (!test) {
        printf("Test №1 FAILED");
    } else {
        printf("Test №1 PASSED");
    }
    _free(test);

    munmap(heap, heap_size);
}

void test_2() {
    void *heap = heap_init(REGION_MIN_SIZE);
    size_t const heap_size = region_actual_size(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *m1 = _malloc(2048);
    void *m2 = _malloc(2048);

    void *const m2_addr_copy = m2;

    void *m3 = _malloc(2048);
    debug_heap(stdout, heap);

    _free(m2);

    m2 = _malloc(1024);
    debug_heap(stdout, heap);

    if (m2 == m2_addr_copy) {
        printf("Test №2 PASSED");
    } else {
        printf("Test №2 FAILED");
    }

    _free(m1);
    _free(m2);
    _free(m3);

    munmap(heap, heap_size);
}

void test_3() {
    void *heap = heap_init(REGION_MIN_SIZE);
    size_t const heap_size = region_actual_size(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *m1 = _malloc(2048);
    void *m2 = _malloc(2048);

    void *const m2_addr_copy = m2;

    void *m3 = _malloc(2048);

    void *const m3_addr_copy = m3;
    debug_heap(stdout, heap);

    _free(m2);
    _free(m3);

    m2 = _malloc(2048);
    m3 = _malloc(1488);
    debug_heap(stdout, heap);

    if (m2 == m2_addr_copy && m3 == m3_addr_copy) {
        printf("Test №3 PASSED");
    } else {
        printf("Test №3 FAILED");
    }

    _free(m1);
    _free(m2);
    _free(m3);

    munmap(heap, heap_size);
}

void test_4() {
    void *heap = heap_init(REGION_MIN_SIZE);
    size_t const heap_size = region_actual_size(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *m = _malloc(heap_size * 2);
    debug_heap(stdout, heap);

    if (m == HEAP_START + offsetof(struct block_header, contents)) {
        printf("Test №4 PASSED");
    } else {
        printf("Test №4 FAILED");
    }
    _free(m);

    munmap(heap, heap_size * 2);
}

void test_5() {
    void *heap = heap_init(REGION_MIN_SIZE);
    size_t const heap_size = region_actual_size(REGION_MIN_SIZE);
    debug_heap(stdout, heap);

    void *after_heap_addr = mmap(heap + region_actual_size(REGION_MIN_SIZE),
                                 REGION_MIN_SIZE,
                                 PROT_READ | PROT_WRITE,
                                 MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE,
                                 -1,
                                 0);
    if (after_heap_addr == MAP_FAILED) {
        printf("Something went wrong with mmap");
        return;
    }

    void *m = _malloc(heap_size * 2);

    debug_heap(stdout, heap);

    if (m == heap + offsetof(struct block_header, contents)) {
        printf("Test №5 FAILED");
    } else {
        printf("Test №5 PASSED");
    }

    _free(m);

    munmap(heap, heap_size * 2);
    munmap(m, region_actual_size(heap_size * 2));
}
